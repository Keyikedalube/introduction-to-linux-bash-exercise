#!/bin/bash

month=$1
monthLen=${#month}

# Make sure argument is provided
if [ $monthLen -eq 0 ] ; then
    echo 'Enter a month in numerical form (i.e., 1-12) as argument'
    exit
fi

case "$month" in
    1) echo January;;
    2) echo February;;
    3) echo March;;
    4) echo April;;
    5) echo May;;
    6) echo June;;
    7) echo July;;
    8) echo August;;
    9) echo September;;
    10) echo Octorber;;
    11) echo November;;
    12) echo December;;
    *)
	if [ $month -eq 0 ] ; then
	    echo A month cannot start from zero!
	elif [ $month -lt 0 ] ; then
	    echo A month cannot be in negative!
	elif [ $month -gt 12 ] ; then
	    echo A month cannot be more than 12!
	else
	    echo Wrong input!
	fi
esac
