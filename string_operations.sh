#!/bin/bash

fstArg=$1
scdArg=$2

fstArgLen=${#fstArg}
scdArgLen=${#scdArg}

# Determine if first argument is of zero length
if [ $fstArgLen -eq 0 ] ; then
    echo The first argument is of zero length
else
    echo The first argument is not of zero length
fi

# Determine if second argument is of non-zero lengthn
if [ $scdArgLen -ne 0 ] ; then
    echo The second argument is of non-zero length
else
    echo The second argument is not of non-zero length
fi

# Print out and compare between the two string's length
echo First argument length: $fstArgLen
echo Second argument length: $scdArgLen

if [ $fstArgLen -lt $scdArgLen ] ; then
    echo First argument is smaller than Second argument
elif [ $fstArgLen -eq $scdArgLen ] ; then
    echo First and Second argument are equal in length
else
    echo First argument is larger than Second argument
fi

# Now compare the strings to see if they are same and report the result
if [[ $fstArg == $scdArg ]] ; then
   echo The two strings are same
else
    echo The two strings are not same
fi
