#!/bin/bash

echo Enter a number \(1 or 2\):
read NUMBER

# Check if it is 1 or 2, otherwise report an error
if [ $NUMBER -eq 1 ]
then
    export env_var=yes
else
    if [ $NUMBER -eq 2 ]
    then
	export env_var=no
    else
	echo The number cannot be other than 1 or 2!
	export env_var=unknown!
    fi
fi

# Display the environment variable
echo The value of env_var is $env_var

       
