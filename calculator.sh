#!/bin/bash

error () {
    echo Improper use of script    
    echo Usage is:
    echo "$0 <operation> <operand1> <operand2>"
    echo
    echo Where \<operation\> may be
    echo
    echo "    a = addition"
    echo "    s = subtraction"
    echo "    m = multiplication"
    echo "    d = division"
    exit 1
}

if [[ $1 == 'a' ]] ; then
    echo Addition of $2 and $3 is $(expr $2 + $3)
elif [[ $1 == 's' ]] ; then
    echo Subtraction of $2 and $3 is $(($2 - $3))
elif [[ $1 == 'm' ]] ; then
    let x=($2 * $3)
    echo Multiplication of $2 and $3 is $x
elif [[ $1 == 'd' ]] ; then
    let x=($2 / $3)
    echo Division of $2 and $3 is $x
else
    error
fi
