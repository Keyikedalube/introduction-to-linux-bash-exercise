#!/bin/bash

func1 () {
    echo This is func1
}

func2 () {
    echo This is func2
}

func3 () {
    echo This is func3
}

echo Enter a number \(1, 2 or 3\):
read number
func$number
