#!/bin/bash

# Get directory
echo Enter a directory name:
read directory
# Inform directory creation
echo Creating the directory with that name: $directory
mkdir $directory
# Change to newly created directory
cd $directory
# Show the user which directory he/she is in
echo Now you\'re in this directory
pwd
# Create some random files
touch file1 file2 file3 file4 file5
# Show the user that those files are empty
ls -l
# Using redirection, putting contents on the files
echo What a script :P > file1
echo on file2 this too > file2
echo file3 also!!! > file3
echo yet again over here on file4 > file4
echo and it goes on... > file5
# Display the contents of all files
cat *
# Ending process
echo Goodbye user!
# Clean up
# Remove all files within the directory
rm -v *
# Move back one directory
cd ..
# And remove the directory too
rmdir -v $directory

